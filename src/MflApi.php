<?php

namespace MattLibera\MflApi;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\Log;

class MflApi
{

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    // These come straight from the documentation: https://api.myfantasyleague.com/2017/api_info
    protected $protocol;
    protected $host;
    protected $year;

    protected $leagueId;

    protected $proxyHost;
    protected $proxyPort;

    protected $useProxy = false;

    protected $userCookie = [];
    protected $authkey = '';

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * @param $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @param mixed $leagueId
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;
    }

    /**
     * @param mixed $proxyHost
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * @param mixed $proxyPort
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * @param boolean $useProxy
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }

    /**
     * @param boolean $authKey
     */
    public function setAuthKey($authKey)
    {
        $this->authKey = $authKey;
    }

    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to MFL APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * Performs a call to the MFL API
     *
     * @param string $method - call type (get, post, etc.)
     * @param string $command - command type (export, import, etc.)
     * @param string $type - command name (e.g. players, injuries, etc.)
     * @param array $params - arguments / options / etc. to be passed to the call
     * @param string $asUser - (optional) - the username with which to perform the API call, if not the commissioner
     *
     * @return array
     */

    protected function apiCall($method, $command, $type, $params = [], $authMethod = 'key', $asUser = null)
    {
        Log::info('command: ' . $command);

        $requiredProperties = ['protocol', 'host', 'year'];
        foreach ($requiredProperties as $property) {
            if ($this->$property === null) {
                throw new \Exception("Error: required property '$property' has not been set in API object.");
            }
        }

        // assemble the request target and endpoint
        $endpoint = $this->protocol . '://' . $this->host . '/' . $this->year;

        if ($command == 'export') {
            $parseType = 'json'; // json will be returned
            $endpoint .= '/' . $command;

            // add params that are present no matter what
            $defaultParams = [
                "TYPE" => $type,
                "JSON" => 1
            ];

            Log::info('authMethod: ' . $authMethod);
            if ($authMethod == 'key') {
                $defaultParams['APIKEY'] = $this->authkey;
            }

            $params = array_merge($params, $defaultParams);
        } elseif ($command == 'import') {
            // set up to use XML
            $parseType = 'xml';
            $endpoint .= '/' . $command;

            $defaultParams = [
                "TYPE" => $type,
            ];

            if ($authMethod == 'key') {
                $defaultParams['APIKEY'] = $this->authkey;
            }

            $params = array_merge($params, $defaultParams);
        } else {
            $parseType = 'xml'; // xml will be returned
            $endpoint .= '/' . $type;
        }

        // run the call as another user? this is required for some operations (e.g. recording trash)
        if (!is_null($asUser)) {
            // TODO - this
        } else {
            // TODO - this
        }


        // instantiate Guzzle client with headers
        $client = new Client([

        ]);

        // set up basic options
        $requestOptions = [

        ];

        if ($authMethod != 'key' && $type != 'login') {
            $requestOptions['headers'] = [
                'Cookie' => 'MFL_USER_ID=' . $this->userCookie
            ];
            Log::info('cookie: ' . $this->userCookie);
        }

        // params / body
        if (strtolower($method) == 'get') {
            $requestOptions['query'] = $params;
        } else {
            $requestOptions['form_params'] = $params;
        }

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        }

        //        if ($type != 'login') dd($requestOptions);
        //        $requestOptions['debug'] = true;
        //         if ($type != 'login') {
        //             $requestOptions['on_stats'] = function (TransferStats $stats) use (&$url) {
        //                 $url = $stats->getEffectiveUri();
        //             };
        //         }

        // perform the call
        $response = $client->$method($endpoint, $requestOptions);

        // if ($type != 'login') {
        //     dd($url);
        // }
        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = $parseType == 'json' ? json_decode(
            $body->getContents(),
            true
        ) : Helpers::xml2array($body->getContents()); // parse to array (necessary because error text is stored in an object with key of $t)

        // parse basic info for all requests

        //        dd($response);
        // return raw data about the request so that it can be parsed by the calling function
        return [
            'response' => [
                'endpoint'   => $endpoint,
                'type'       => $type,
                'params'     => $params,
                'method'     => $method,
                'result'     => isset($bodyContents['error']) ? 'failure' : 'success',
                'message'    => isset($bodyContents['error']['$t']) ? $bodyContents['error']['$t'] : null,
                'httpCode'   => $code,
                'httpReason' => $reason
            ],
            'body' => $bodyContents
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Authorization / Login
    |--------------------------------------------------------------------------
    |
    | Log a user in (get cookie)
    |
    */

    public function authenticate()
    {
    }

    /*
    |--------------------------------------------------------------------------
    | Leagues
    |--------------------------------------------------------------------------
    |
    | Operations on Leagues
    |
    */

    public function myLeagues()
    {
        return $this->apiCall('get', 'export', 'myleagues');
    }

    public function leagueStandings()
    {
        return $this->apiCall('get', 'export', 'leagueStandings', ['L' => $this->leagueId]);
    }

    public function leagueInfo()
    {
        return $this->apiCall('get', 'export', 'league', ['L' => $this->leagueId]);
    }

    /*
    |--------------------------------------------------------------------------
    | Franchises
    |--------------------------------------------------------------------------
    |
    | Operations on Franchises
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Rosters
    |--------------------------------------------------------------------------
    |
    | Operations on Rosters
    |
    */

    public function allRosters()
    {
        return $this->apiCall('get', 'export', 'rosters', ['L' => $this->leagueId]);
    }

    public function fetchRoster($franchise)
    {
        return $this->apiCall('get', 'export', 'rosters', ['L' => $this->leagueId, 'FRANCHISE' => $franchise]);
    }

    public function updateSalary($playerId, $salary, $contractYears, $contractInfo = '', $contractStatus = '')
    {
        $xml = <<<XML
<salaries>
<leagueUnit unit="LEAGUE">
        <player id="$playerId" salary="$salary" contractYear="$contractYears" contractInfo="$contractInfo" contractStatus="$contractStatus" />
</leagueUnit>
</salaries>
XML;

        return $this->apiCall('post', 'import', 'salaries', ['L' => $this->leagueId, 'DATA' => $xml, 'APPEND' => 1]);
    }

    /*
    |--------------------------------------------------------------------------
    | Players
    |--------------------------------------------------------------------------
    |
    | Operations on Players
    |
    */

    public function allPlayers($details = false)
    {
        if ($details) {
            return $this->apiCall('get', 'export', 'players', ['DETAILS' => 1]);
        }
        return $this->apiCall('get', 'export', 'players');
    }

    public function fetchPlayers($ids, $details = false)
    {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }

        if ($details) {
            return $this->apiCall('get', 'export', 'players', ['PLAYERS' => $ids, 'DETAILS' => 1]);
        }

        return $this->apiCall('get', 'export', 'players', ['PLAYERS' => $ids]);
    }

    public function playersSince($timestamp, $details = false)
    {
        if (!is_int($timestamp)) {
            $timestamp = strtotime($timestamp);
        }

        $this->setHost('api.myfantasyleague.com');

        if ($details) {
            $result = $this->apiCall('get', 'export', 'players', ['SINCE' => $timestamp, 'DETAILS' => 1]);
        } else {
            $result = $this->apiCall('get', 'export', 'players', ['SINCE' => $timestamp]);
        }

        return $result;
    }

    public function promotePlayerFromTaxi($ids)
    {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }

        return $this->apiCall('post', 'import', 'taxi_squad', ['L' => $this->leagueId, 'PROMOTE' => $ids]);
    }

    public function demotePlayerToTaxi($ids)
    {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }
        return $this->apiCall('post', 'import', 'taxi_squad', ['L' => $this->leagueId, 'DEMOTE' => $ids]);
    }

    public function activatePlayerFromIr($ids)
    {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }

        return $this->apiCall('post', 'import', 'ir', ['L' => $this->leagueId, 'ACTIVATE' => $ids]);
    }

    public function deactivatePlayerToIr($ids)
    {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }

        return $this->apiCall('post', 'import', 'ir', ['L' => $this->leagueId, 'DEACTIVATE' => $ids]);
    }

    /*
    |--------------------------------------------------------------------------
    | Injuries
    |--------------------------------------------------------------------------
    |
    | Operations on Injuries
    |
    */

    public function allInjuries($week = false)
    {
        $this->setHost('api.myfantasyleague.com');

        if ($week) {
            return $this->apiCall('get', 'export', 'injuries', ['W' => $week]);
        }
        return $this->apiCall('get', 'export', 'injuries');
    }

    /*
    |--------------------------------------------------------------------------
    | Transactions
    |--------------------------------------------------------------------------
    |
    | League Transactions
    |
    */

    public function transactions($options = [])
    {
        // valid options: W, TRANS_TYPE, FRANCHISE, DAYS, COUNT
        $requestOptions = [
            'L'          => $this->leagueId,
            'W'          => '',
            'TRANS_TYPE' => '',
            'FRANCHISE'  => '',
            'DAYS'       => '',
            'COUNT'      => ''
        ];

        foreach ($requestOptions as $option => $value) {
            if (isset($options[$option])) {
                $requestOptions[$option] = $options[$option];
            }
        }

        return $this->apiCall('get', 'export', 'transactions', $requestOptions);
    }

    /*
    |--------------------------------------------------------------------------
    | NFL Data
    |--------------------------------------------------------------------------
    |
    | Operations on NFL Data
    |
    */

    public function byeWeeks($week = false)
    {
        if ($week) {
            return $this->apiCall('get', 'export', 'nflByeWeeks', ['W' => $week]);
        }
        return $this->apiCall('get', 'export', 'nflByeWeeks');
    }

    // If the week is not specified, it defaults to the current week. If set to 'ALL', it returns the full season schedule.
    public function gameData($week = false)
    {
        $this->setHost('api.myfantasyleague.com');

        if ($week) {
            return $this->apiCall('get', 'export', 'nflSchedule', ['W' => $week]);
        }
        return $this->apiCall('get', 'export', 'nflSchedule');
    }

    /*
    |--------------------------------------------------------------------------
    | Scoring
    |--------------------------------------------------------------------------
    |
    | Operations on player scoring
    |
    */

    public function scoresForWeek($week, $year)
    {
        return $this->apiCall('get', 'export', 'playerScores', ['W' => $week, 'L' => $this->leagueId, 'YEAR' => $year]);
    }

    public function scoresYtd($year)
    {
        return $this->scoresForWeek('YTD', $year);
    }

    /*
    |--------------------------------------------------------------------------
    | Matchups and Results
    |--------------------------------------------------------------------------
    |
    | Operations on league matchups and results
    |
    */

    public function schedule($week = null, $franchise = null)
    {
        $options = ['L' => $this->leagueId];

        if (!is_null($week)) {
            $options['W'] = $week;
        }

        if (!is_null($franchise)) {
            $options['F'] = $franchise;
        }

        return $this->apiCall('get', 'export', 'schedule', $options);
    }

    public function leagueResults($week = null)
    {
        $options = ['L' => $this->leagueId];
        if (!is_null($week)) {
            $options['W'] = $week;
        }
        return $this->apiCall('get', 'export', 'weeklyResults', $options);
    }

    public function playoffBrackets()
    {
        $options = ['L' => $this->leagueId];
        return $this->apiCall('get', 'export', 'playoffBrackets', $options);
    }

    public function playoffBracket($id)
    {
        $options = [
            'L'          => $this->leagueId,
            'BRACKET_ID' => $id
        ];
        return $this->apiCall('get', 'export', 'playoffBracket', $options);
    }

    public function liveScoring($week = null)
    {
        $options = ['L' => $this->leagueId];

        if (! is_null($week)) {
            $options['W'] = $week;
        }

        return $this->apiCall('get', 'export', 'liveScoring', $options);
    }

    /*
    |--------------------------------------------------------------------------
    | Assets
    |--------------------------------------------------------------------------
    |
    | Operations on league assets (tradable things)
    |
    */

    public function listAssets()
    {
        $options = ['L' => $this->leagueId];
        return $this->apiCall('get', 'export', 'assets', $options);
    }

    /*
    |--------------------------------------------------------------------------
    | Free Agents
    |--------------------------------------------------------------------------
    |
    | Operations on league free agents
    |
    */

    public function listFreeAgents($position = null)
    {
        $options = ['L' => $this->leagueId];
        if (!is_null($position)) {
            $options['POSITION'] = $position;
        }
        return $this->apiCall('get', 'export', 'freeAgents', $options);
    }
}

# MyFantasyLeague (MFL) API PHP Library

Contact: matt@mattlibera.com

# Introduction

This package is a PHP library used to interact with the MyFantasyLeague (MFL) API, documented here: [https://api.myfantasyleague.com/2017/api_info](https://api.myfantasyleague.com/2017/api_info).

> This is a **work in progress**. Not recommend for production apps just yet.

## Scope

This package is built not (yet) as a comprehensive interface with the MFL API - it is not written to perform every conceivable API call to MFL. Specifically, this package was built to suit the needs of a particular league that I am commissioning, which uses advanced features that MFL cannot natively support.

Please fork and add methods to this as you see fit / as your needs dictate.

# Installation

1. `composer require 'mattlibera/mfl-api-php-library'`
2. Use one of the API calls built into the `MattLibera\MflApi\MflApi` class, or extend it and add your own. The base API call is built in... you'll have to find a way in whatever framework / app you're working in to call the setter methods to set the variables required to make the call - specific to your environment (e.g. urls, credentials, etc.)

# Version History

### 0.6

- Guzzle 7, PHP 8
### 0.5

- Free Agents

### 0.4

- Adds asset retrieval

### 0.3.1

- Composer data updates, including PSR-4

### 0.3

- Adds playoff bracket retrieval

### 0.2

- Adds contract update method

### 0.1

Getting started. Available functionality:

- API call abstraction and laying groundwork

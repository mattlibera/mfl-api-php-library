<?php
//Perform a request, optionally including a body, returning an object containing the HTTP response code and response body
//A return code of 0 indicates that the request could not be completed for some reason
//function request($username, $password, $url, $input = null)
//{
//    $response = array('data' => false, 'error' => '', 'code' => 0);
//    $curl = curl_init();
//    if (is_resource($curl))
//    {
//        //Turn on debugging (if you need it), comment out if you don't require it
//        curl_setopt($curl, CURLOPT_VERBOSE, true);
////        curl_setopt($curl, CURLOPT_STDERR, fopen('php://output', 'w+')); // matt added this to see info on screen.
//
//        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); //Maximum verification
//        //Force TLS 1.2 because MEETS SaaS supports it
//        curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
//        //Force strong hash algorithms because MEETS SaaS support them (string below is for OpenSSL / LibreSSL only)
//        curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, '-ALL:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256');
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
//        //Blindly following location is extremely dangerous with Basic Authentication, ensure it's off
//        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
//        curl_setopt($curl, CURLOPT_URL, $url);
//        if ($input !== null)
//        {
//            curl_setopt($curl, CURLOPT_POST, true);
//            curl_setopt($curl, CURLOPT_POSTFIELDS, $input);
//        }
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        $response['data'] = curl_exec($curl);
//        if ($response['data'] !== false)
//        {
//            $response['code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//        }
//        else
//        {
//            $response['error'] = curl_error($curl);
//        }
//        curl_close($curl);
//    }
//    return (object)$response;
//}
//



require('creds.php');
require('src/MflApi.php');
require('src/Helpers.php');
require('vendor/autoload.php');

$mflApi = new \MattLibera\MflApi\MflApi();

$mflApi->setYear('2017');
$mflApi->setProtocol('https');
$mflApi->setHost('www64.myfantasyleague.com');
$mflApi->setLeagueId($leagueId);

//$result = $mflApi->allPlayers();
//$result = $mflApi->myLeagues();
//$result = $mflApi->authenticate($username, $password);
//$result = $mflApi->leagueStandings();
//$result = $mflApi->leagueInfo();
$result = $mflApi->rosters();

?>

<pre>
    <?php var_dump($result); ?>
</pre>